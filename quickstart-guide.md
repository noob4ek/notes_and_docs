# Руководство пользователя по быстрому развертыванию инфраструктуры

## 1 Этап - Заведение бакета S3 для хранения бэкапов
---
Необходимая ручная операция, для создания бакета для бэкапов

1. Проходим в личный кабинет VKCS
2. Создаем бакет согласно [инструкции](https://mcs.mail.ru/docs/base/s3/quick-start#1--sozdayte-baket), с той лишь разницей что нам подойдет и ice box
3. Переходим на раздел с созданным бакетом
4. Жмем на вкладку ключи, а после на добавить ключ
5. Вводим имя и ждем создать, и получаем пободную картину:

![S3 Ключи](files/s3-key.png)

6. Скопируем данные для дальнейшего использования
7. Можем переходить к следующему этапу

---

## 2 Этап - Заполнение данных в переменных CI
---

1. В проекте инфраструктуры необходимо заполнить переменные описанные в [README](https://gitlab.com/gb_final_project/infrastructure/-/blob/master/README.md)
2. Также введенные переменные CI необходимо заполнить и в [проекте с приложением](https://gitlab.com/gb_final_project/application)


## 3 Этап - Запуск джобы на создание инфраструктуры
- В случае если первоначальный запуск инфраструктуры, необходимо запустить джобу по разворачиванию инфраструктуры (terraform-deploy), при этом на выходе из нее получить IP адреса для инфраструктуры, и параметры подключения к шаре.
Это можно посмотреть в пайплайнах. После следует настроить DNS записи на стророне регистранта, и внести параметры подключения к шаре, в переменные CI в [папке с приложением](https://gitlab.com/gb_final_project/application)

- В случае если джоба уже была запущена и инфраструктура не пересоздавалась полностью включая и сеть, то нужно дождаться приведения ее в боевое состояние и приступать к следующему шагу.
Для этого на вкладке environments нужно произвести ее редеплой

## 4 Этап - Запуск джобы на конфигурирувание инфраструктуры

Для запуска джобы на конфигурирование инфраструктуры нужно запустить ручную джобу ansible-run-all в проекте с инфраструктурой

## 5 Этап - Запуск джобы на деплой приложения

Для запуска джобы на разворачивание приложения нужно в проекте с приложением запустить environment, это развернет приложение

## 6 Этап (опциональный настройка ботов и уведомлений в телеграм)
---
За деталями обращайтесь на соответсвующий [раздел инструкции](bots-instructions.md)

