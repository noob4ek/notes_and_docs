# Дэшборды и инструкция по пользованию

Ссылка на [Grafana](https://mon.gb.janassov.kz/dashboards)

Логин: demo-gb

Пароль: demo-gb

## Имеются следующие дэшборды:
* [Node Exporter Full](https://mon.gb.janassov.kz/d/rYdddlPWk/node-exporter-full?orgId=1) - содержит общую инфромацию о машине с метриками по нагрузке, использованию памяти и тд:
![Node Exporter Full](files/ne-full.png)

* [MySQL Overview](https://mon.gb.janassov.kz/d/MQWgroiiz/mysql-overview?orgId=1&refresh=1m) - содержит статистику по MySQL, нагрузке, используемой памяти и так далее: 
![MySQL Overview](files/mysql.png)

* [Balancer Nginx Stats](https://mon.gb.janassov.kz/d/4DFTt9Wnky/balancer-nginx-stats?orgId=1) - содержит статистику по NGINX - колличество ответов 2**, 3**, 4**, 5**
![Balancer Nginx Stats](files/nginx.png)
